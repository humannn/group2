﻿using System;

namespace grp2
{
    class Program
    {
        static void Main(string[] args)
        {
            int b = 0;
            int sum = 0;
            Console.WriteLine("Please enter a number: ");
            int a = Convert.ToInt32(Console.ReadLine());

            while (b < a)
            {
                b++;
                Console.WriteLine(b);
                sum += b;
            }

            Console.WriteLine("The sum is: " + sum);

        }
    }
}
